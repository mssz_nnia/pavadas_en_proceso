# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Usuario\Desktop\Funciones\dialogo.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        Dialog.setMinimumSize(QtCore.QSize(400, 300))
        Dialog.setMaximumSize(QtCore.QSize(400, 300))
        self.leNroArt = QtWidgets.QLineEdit(Dialog)
        self.leNroArt.setGeometry(QtCore.QRect(170, 90, 191, 20))
        self.leNroArt.setObjectName("leNroArt")
        self.leDesc = QtWidgets.QLineEdit(Dialog)
        self.leDesc.setGeometry(QtCore.QRect(170, 130, 191, 20))
        self.leDesc.setObjectName("leDesc")
        self.etqNroArt = QtWidgets.QLabel(Dialog)
        self.etqNroArt.setGeometry(QtCore.QRect(36, 90, 111, 20))
        self.etqNroArt.setMinimumSize(QtCore.QSize(111, 20))
        self.etqNroArt.setMaximumSize(QtCore.QSize(111, 20))
        self.etqNroArt.setObjectName("etqNroArt")
        self.etqDesc = QtWidgets.QLabel(Dialog)
        self.etqDesc.setGeometry(QtCore.QRect(36, 130, 111, 20))
        self.etqDesc.setMinimumSize(QtCore.QSize(111, 20))
        self.etqDesc.setMaximumSize(QtCore.QSize(111, 20))
        self.etqDesc.setObjectName("etqDesc")
        self.etqCodigo = QtWidgets.QLabel(Dialog)
        self.etqCodigo.setGeometry(QtCore.QRect(36, 50, 111, 20))
        self.etqCodigo.setObjectName("etqCodigo")
        self.leCodigo = QtWidgets.QLineEdit(Dialog)
        self.leCodigo.setEnabled(False)
        self.leCodigo.setGeometry(QtCore.QRect(170, 50, 191, 20))
        self.leCodigo.setMinimumSize(QtCore.QSize(191, 20))
        self.leCodigo.setMaximumSize(QtCore.QSize(191, 20))
        self.leCodigo.setObjectName("leCodigo")
        self.btnConfirmar = QtWidgets.QPushButton(Dialog)
        self.btnConfirmar.setGeometry(QtCore.QRect(170, 220, 75, 23))
        self.btnConfirmar.setMinimumSize(QtCore.QSize(75, 23))
        self.btnConfirmar.setMaximumSize(QtCore.QSize(75, 23))
        self.btnConfirmar.setObjectName("btnConfirmar")
        self.btnCancelar = QtWidgets.QPushButton(Dialog)
        self.btnCancelar.setGeometry(QtCore.QRect(290, 220, 75, 23))
        self.btnCancelar.setObjectName("btnCancelar")
        self.lePrecio = QtWidgets.QLineEdit(Dialog)
        self.lePrecio.setGeometry(QtCore.QRect(170, 170, 191, 20))
        self.lePrecio.setObjectName("lePrecio")
        self.etqPrecio = QtWidgets.QLabel(Dialog)
        self.etqPrecio.setGeometry(QtCore.QRect(40, 170, 111, 20))
        self.etqPrecio.setMinimumSize(QtCore.QSize(111, 20))
        self.etqPrecio.setMaximumSize(QtCore.QSize(111, 20))
        self.etqPrecio.setObjectName("etqPrecio")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.etqNroArt.setText(_translate("Dialog", "Nro. Articulo"))
        self.etqDesc.setText(_translate("Dialog", "Descripcion"))
        self.etqCodigo.setText(_translate("Dialog", "Codigo"))
        self.btnConfirmar.setText(_translate("Dialog", "Confirmar"))
        self.btnCancelar.setText(_translate("Dialog", "Cancelar"))
        self.etqPrecio.setText(_translate("Dialog", "Precio"))