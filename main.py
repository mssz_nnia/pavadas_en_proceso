import sqlite3, sys

from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QApplication

from PyQt5.QtCore import QModelIndex

from agrega import dialogVentana

from tabla import Ui_MainWindow

class mainVentana(QMainWindow):

    def __init__(self):

        super(mainVentana, self).__init__()

        self.ui = Ui_MainWindow()

        self.ui.setupUi(self)

        self.ui.tableWidget.setRowCount(9);

        self.ui.tableWidget.setColumnCount(4)

        self.ui.btnAgregar.clicked.connect(self.abrirDlg)

        self.ui.btnVer.clicked.connect(self.verTableWidget)

        self.ui.btEliminar.clicked.connect(self.borrarFila)

        
    def abrirDlg(self):

        self.dlg_uno = dialogVentana()


    def verTableWidget(self, conn):

        conn = sqlite3.connect( ) #Conectar base

        curs = conn.cursor()

        curs.execute('SELECT * FROM articulo')

        respuesta = curs.fetchall()

        fila = 0

        for tupla in respuesta:

            columna = 0

            for item in tupla:

                celda_info = QTableWidgetItem(f"{item}") #F-String(actualizado) De Integer a String

                self.ui.tableWidget.setItem(fila, columna, celda_info)

                columna+=1

            fila+=1

        conn.commit()

        conn.close()


    def borrarFila(self):

        self.selected = self.ui.tableWidget.currentIndex()

        if not self.selected.isValid() or len(self.ui.tableWidget.selectedItems()) < 1:

            return

        self.items = self.ui.tableWidget.selectedItems()[0]

        self.idls = self.items.text()


        conn = sqlite3.connect( ) #Conectar base

        curs = conn.cursor()

        curs.execute('DELETE FROM articulo WHERE codigo like ?', self.idls)

        self.ui.tableWidget.removeRow(self.selected.row())

        self.ui.tableWidget.setCurrentIndex(QModelIndex())

        conn.commit()

        conn.close()


if __name__ == "__main__":

    app = QApplication(sys.argv)

    principal = mainVentana()

    principal.show()

    sys.exit(app.exec_())