# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Usuario\Desktop\Pruebas\Funciones\tabla.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(30, 120, 731, 311))
        self.tableWidget.setMinimumSize(QtCore.QSize(731, 311))
        self.tableWidget.setMaximumSize(QtCore.QSize(731, 311))
        self.tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        self.tableWidget.verticalHeader().setVisible(False)
        self.btnAgregar = QtWidgets.QPushButton(self.centralwidget)
        self.btnAgregar.setGeometry(QtCore.QRect(320, 470, 75, 23))
        self.btnAgregar.setMinimumSize(QtCore.QSize(75, 23))
        self.btnAgregar.setMaximumSize(QtCore.QSize(75, 23))
        self.btnAgregar.setObjectName("btnAgregar")
        self.btnVer = QtWidgets.QPushButton(self.centralwidget)
        self.btnVer.setGeometry(QtCore.QRect(430, 470, 75, 23))
        self.btnVer.setMinimumSize(QtCore.QSize(75, 23))
        self.btnVer.setMaximumSize(QtCore.QSize(75, 23))
        self.btnVer.setObjectName("btnVer")
        self.btEliminar = QtWidgets.QPushButton(self.centralwidget)
        self.btEliminar.setGeometry(QtCore.QRect(190, 470, 75, 23))
        self.btEliminar.setMinimumSize(QtCore.QSize(75, 23))
        self.btEliminar.setMaximumSize(QtCore.QSize(75, 23))
        self.btEliminar.setObjectName("btEliminar")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Codigo"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Nro_articulo"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Descripcion"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Precio"))
        self.btnAgregar.setText(_translate("MainWindow", "Agregar"))
        self.btnVer.setText(_translate("MainWindow", "Ver"))
        self.btEliminar.setText(_translate("MainWindow", "Eliminar"))