import sqlite3

from PyQt5.QtWidgets import QDialog

from dialogo import Ui_Dialog

class dialogVentana(QDialog):

    def __init__(self):

        super(dialogVentana, self).__init__()

        self.ui = Ui_Dialog()

        self.ui.setupUi(self)

        self.mostrarDialogo()

        self.ui.btnConfirmar.clicked.connect(self.crearRegistro)

    
    def crearRegistro(self):

        self.insertarArticulo = [

            None,
            self.ui.leNroArt.text(),
            self.ui.leDesc.text(),
            self.ui.lePrecio.text()]

        conn = sqlite3.connect( ) #Conectar base

        curs = conn.cursor()

        curs.execute('INSERT INTO articulo VALUES (?,?,?,?)', self.insertarArticulo)

        conn.commit()

        conn.close()

    def mostrarDialogo(self):

        self.show()